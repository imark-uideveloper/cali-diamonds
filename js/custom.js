/*  Search-bar  */
 jQuery(function() {
  jQuery('a[href="#search"]').on("click", function(event) {
    event.preventDefault();
    jQuery("#search").addClass("open");
    jQuery('#search > form > input[type="search"]').focus();
  });

  jQuery("#search_btn").on("click", function(event) {
     event.preventDefault();
     jQuery('#search').removeClass("open");
  });

  jQuery("form").submit(function(event) {
    event.preventDefault();
    return false;
  });
});

/* // Search-bar  */

/* Wow Animation*/
new WOW().init();
/* //Wow Animation*/

/* Animated-Scroll-Bar */ 
jQuery(window).scroll(function () {
		var sc = $(window).scrollTop()
		if (sc > 100) {
			$("#header-sroll").addClass("small")
		} else {
			$("#header-sroll").removeClass("small")
		}
	});
/* //Animated-Scroll-Bar */ 

jQuery('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        600:{
            items:1,
            nav:false
        },
        1000:{
            items:1,
            nav:true,
            loop:false
        }
    }
})

/*  Image-Hover */
jQuery(document).ready(function() {
  jQuery("#cf7_controls").on('click', 'span', function() {
    jQuery("#cf7 img").removeClass("opaque");

    var newImage = $(this).index();

    jQuery("#cf7 img").eq(newImage).addClass("opaque");

    jQuery("#cf7_controls span").removeClass("selected");
    jQuery(this).addClass("selected");
  });
});

/* = For-First-Cap Capital =- */
jQuery('.firstCap').on('keypress', function(event) {
    var $this = $(this),
    thisVal = $this.val(),
    FLC = thisVal.slice(0, 1).toUpperCase();
    con = thisVal.slice(1, thisVal.length);
    jQuery(this).val(FLC + con);
});

/*= Bootstrap-DropDown-Menu ==*/

/*jQuery(document).ready(function(){
    jQuery(".dropdown").hover(            
        function() {
            jQuery('.dropdown-menu', this).not('.in .dropdown-menu').stop(true,true).slideDown("400");
            jQuery(this).toggleClass('open');        
        },
        function() {
            jQuery('.dropdown-menu', this).not('.in .dropdown-menu').stop(true,true).slideUp("400");
            jQuery(this).toggleClass('open');       
        }
    );
});*/
$('ul.nav li.dropdown').hover(function() {
  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(400);
}, function() {
  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
});


/*  Flexslider  */

jQuery(function(){
    jQuery(window).load(function(){
      jQuery('#carousel').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        itemMargin: 7,
        asNavFor: '#slider'
      });

      jQuery('#slider').flexslider({
        animation: "slide",
        controlNav: false,
        animation: "fade",  
        animationLoop: false,
        slideshow: false,
        sync: "#carousel",
        start: function(slider){
          jQuery('body').removeClass('loading');
        }
      });
    });
    
});
/* -=-=-=- */

/*  Image Hover Sound Effect  */
var light = $('');

var tnatn = $('.soundEffect');

var active = false;

/*var snd_active = 'http://soundbible.com/mp3/Wind-Mark_DiAngelo-1940285615.mp3';*/
var snd_active = 'http://www.phptest.g6.cz/CasualResources/LightSaberSnds/Close.mp4';

/*var snd_deactive = 'http://www.phptest.g6.cz/CasualResources/LightSaberSnds/Close.mp4';*/

/*var snd_deactive = 'http://www.phptest.g6.cz/CasualResources/LightSaberSnds/Open.mp4';*/

tnatn.hover(function(){
  if(!active) {
    light.removeClass('deactive');
    light.addClass('active');
    active = true;
    PlaySound(snd_active);
  }
  else {
    active = false;
    light.removeClass('active');
    light.addClass('deactive'); 
    PlaySound(snd_deactive);
  }
});

function PlaySound(url) {
  var audio = new Audio(url);  
  audio.play();
}

/*  //Image Hover Sound Effect  */

